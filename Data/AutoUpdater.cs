﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using FtpLib;
using System.Diagnostics;

namespace Data
{
    public class AutoUpdater
    {
        private string FilePath { get; set; }
        private int CurrentMajorVersion { get; set; }
        private int CurrentMinorVersion { get; set; }
        private int CurrentRevisionVersion { get; set; }
        private int CurrentBuildRevision { get; set; }
        private List<string> PatchesToDownload { get; set; }
        private FtpConnection ftp { get; set; }
        private string FTPUsername { get { return "eriftp"; } }
        private string FTPPassword { get { return "beardWhisker2"; } }

        public AutoUpdater()
        {
            FilePath = Directory.GetCurrentDirectory();
            CurrentMajorVersion = 0;
            CurrentMinorVersion = 0;
            CurrentRevisionVersion = 0;
            CurrentBuildRevision = 0;
            PatchesToDownload = new List<string>();
            //ftp = new FtpConnection("192.168.1.100");
            ftp = new FtpConnection("www.forecourse.com");

            GetFileVersion();
        }

        private void GetFileVersion()
        {
            string version = String.Empty;

            using (TextReader reader = new StreamReader(FilePath + "\\build.txt"))
            {
                version = reader.ReadLine();
            }

            //set current version numbers
            int i = 0;
            foreach (string s in version.Split('.'))
            {
                switch (i++)
                {
                    case 0:
                        CurrentMajorVersion = Convert.ToInt32(s);
                        break;
                    case 1:
                        CurrentMinorVersion = Convert.ToInt32(s);
                        break;
                    case 2:
                        CurrentRevisionVersion = Convert.ToInt32(s);
                        break;
                    case 3:
                        CurrentBuildRevision = Convert.ToInt32(s);
                        break;
                }
            }
            
        }

        private bool CompareVersions(string patchVersion)
        {
            int i = 0;
            int major = 0;
            int minor = 0;
            int revision = 0;
            int build = 0;
            foreach (string s in patchVersion.Split('.'))
            {
                switch (i++)
                {
                    case 0:
                        major = Convert.ToInt32(s);
                        break;
                    case 1:
                        minor = Convert.ToInt32(s);
                        break;
                    case 2:
                        revision = Convert.ToInt32(s);
                        break;
                    case 3:
                        build = Convert.ToInt32(s);
                        break;
                }
            }

            if ((major >= CurrentMajorVersion &&
                minor >= CurrentMinorVersion &&
                revision >= CurrentRevisionVersion &&
                build >= CurrentBuildRevision) &&
                (major + "." + minor + "." + revision + "." + build) != 
                 CurrentMajorVersion + "." + CurrentMinorVersion + "." + CurrentRevisionVersion + "." + CurrentBuildRevision)
            {
                return true;
            }

            return false;
        }

        public void Open()
        {
            ftp.Open();
            //ftp.Login();
            ftp.Login(FTPUsername, FTPPassword);
            ftp.SetLocalDirectory(FilePath);
        }

        public void Close()
        {
            ftp.Close();
        }

        public bool CheckForPatches()
        {
            //Root directory
            foreach (FtpFileInfo f in ftp.GetFiles())
            {
                string filename = f.Name;

                string version = filename.Split('-')[1];
                if (CompareVersions(version))
                    PatchesToDownload.Add(f.Name);
            }

            if (PatchesToDownload.Count < 1)
                return false;

            return true;
        }

        public bool DownloadPatches()
        {
            foreach (string p in PatchesToDownload)
            {
                ftp.GetFile(p, FilePath + "\\" + p, false);
            }

            InstallPatches();

            return true;
        }

        private void InstallPatches()
        {

            foreach (string p in PatchesToDownload)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo(
                "cmd.exe",
                string.Format("/c start /MIN /wait msiexec.exe /p {0} REINSTALL=ALL REINSTALLMODE=omus /qn", FilePath + "\\" + p));
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;

                Process proc = Process.Start(startInfo);
                proc.WaitForExit();
            }
        }

        public void LaunchGame()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(
                FilePath + "\\Binaries\\UDK.exe",
                String.Empty);
            startInfo.WindowStyle = ProcessWindowStyle.Normal;

            Process proc = Process.Start(startInfo);
            proc.WaitForExit();
        }
    }
}
