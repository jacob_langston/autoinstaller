﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Security.Cryptography;
using Data;

//http://starksoftftps.codeplex.com/

namespace Checksum
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Data.AutoUpdater c { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            DragMove();
        }

        private void play_Click(object sender, RoutedEventArgs e)
        {
            c = new Data.AutoUpdater();

            c.Open();
            if (c.CheckForPatches())
            {
                messages.Content = "Downloading and installing patches.  Please wait ...";
                c.DownloadPatches();
                messages.Content = "Warm Gun is up to date.  Press play to launch the game.";
            }

            c.LaunchGame();
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            if (e.ButtonState == MouseButtonState.Pressed)
                DragMove();
        }

        private void exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }  

    }
}
